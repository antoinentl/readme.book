# Notes de préparation

## Mots clés

- indépendance
- modularité
- convivialité
- inscrire
- écrire
- éditer
- publier
- transmettre
- sémantique
- structurer
- légèreté
- accessibilité
- autorité
- validation
- conformité
- norme
- fixer
- progresser
- pyramide

## Blocs

### C'est quoi un livre ?
Forme, diffusion, accessibilité.

>Un livre est un contenu clos, fixé et transmissible.

### Qu'est-ce qu'écrire et éditer signifie ?
Inscrire, rédiger, intégrer (code, médias).

Relire, corriger, faire circuler.

>Never go back.

### Les contraintes de la publication ?
Rendre public, validation, autorité.

Portabilité, lisibilité, accessibilité (accéder et lire).

### Et si on parlait convivialité ?
Ne pas dépendre d'*un* outil. Inclusivité en termes de formats et de logiciels.

Pouvoir construire un workflow ou s'y connecter.


## Exemples

### Getty Publications et Quire
Baser une chaîne de publication sur le trio Markdown, Hugo et Prince XML. Markdown permet d'écrire (avec la gestion de métadonnées avec YAML ou TOML), Hugo permet de générer les différents formats (en plus de gérer l'organisation des fichiers)


## Questions en vrac

Comment transformer la pyramide en schéma distribué tout en fixant des contenus ?

Est-ce que la modularité issue des modèles présentés


## Notes en vrac

### Écriture
Question du format.

### Modularité
La base c'est le format dans lequel on stocke le contenu. Même format texte ouvert, différents outils

Ne pas choisir une boîte noire, on peut choisir les outils sans y être contraint.

Git (ou Dropbox, c'est possible).

### Indépendance
Conséquence de la modularité, choix de l'outil.

### Convivialité + mixer
Chacun-e est le ou la bienvenue, l'accès aux outils est identique pour tous le monde.

### Mixer
Possibilité de modifier les rôles.

### Fixation
?

### Autorité
Si livre imprimé, alors reconnaissance. La publication sous une forme donne lieu à d'autres activités.

Exemples de Ninja Squad avec ??? et les activités des auteurs (lecture, intervention, signature).

### Publication en continu
Écrire et publier vite. Feuilleton, magazine. Pre-print (fixation), circulation du texte avant édition finale.

### Automatisation
automatisation, processus, séparation de différents types de contenu (par exemple du code), CI


### Fonctionnel
Installer un livre pour tester du code.

### Éditeur
Rendre un texte légitime.

chaîne de service

fixation pre-print → publication en continu

dissémination, explosion

sous-traitance

éditeur → auto-éditeur

passif-actif, fonctionnel

multidisciplinarité, étanchéité, dette technique, apprentissage (apprendre en voyant faire)

écriture, forme, outils

formats de contenu : formats d'images par exemple

énergie : écrire par fragments, "un readme prend moins de temps qu'une documentation". distinction entre livre (format clot) et documentation (écriture continue), livrer plus tôt  
