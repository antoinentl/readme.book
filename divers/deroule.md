# Déroulé

## Présentation de Thomas et Antoine
Thomas, Antoine.

## Deux définitions

### Livre
Le livre est un objet clos et fixe, que ce soit un livre imprimé (*papier*) ou un livre numérique. Il faut faire une petite distinction entre un livre et une documentation : une documentation est un contenu beaucoup moins figé qu'un livre, un livre s'arrête alors qu'une documentation est continue (en théorie).

Quels livres ? De la fiction (littérature, roman), de l'édition universitaire et des publications professionnelles.

### Chaîne de publication
Une chaîne de publication ou d’édition est l'ensemble des processus et outils permettant de publier ou d'éditer un livre. Une chaîne de publication a pour objectif de gérer des contenus depuis le manuscrit d’un auteur jusqu’à la publication imprimée ou numérique d’un ouvrage, en passant par les phases de relecture, de structuration et de mise en forme. Cette chaîne, ce workflow, est le noyau technique d’une maison d’édition lui permettant de manipuler des textes et de leur donner vie sous forme de livre.


## Écriture
Question du format, de comment on écrit : on peut parler de confort d'écriture, d'environnement d'écriture, etc. Parler d'écriture c'est parler de formats, et plutôt que d'utiliser des formats de traitement de texte, on peut se tourner vers des langages de balisage léger. Il ne s'agit pas d'écrire en HTML, mais plutôt en Markdown ou en Asciidoc.

L'outil n'est pas neutre, surtout si l'outil est une boîte noire.

{exemples dans la partie modularité}

## Modularité
Écrire, mettre en forme et publier nécessite des outils : traitement de texte, logiciel de publication assistée par ordinateur, etc. Si on base une chaîne de publication sur des formats ouverts comme Markdown ou Asciidoc, il est possible d'utiliser tout type d'outils, et d'envisager la constitution d'un environnement *modulaire* : assembler des outils, des briques.

On oublie le schéma à base de boîtes noires, on peut choisir les outils sans y être contraint.

Deux exemples :

- Getty Publications avec une chaîne basée sur un langage de balisage léger (Markdown), un générateur de site statique (Hugo), des métadonnées aux formats YAML ou TOML, et PrinceXML pour la génération de PDF ;
- le livre de Thomas est écrit en utilisant Asciidoc, NodeJS, AsciiDoctor. Donc écriture avec Asciidoc, génération de la version web avec NodeJS, Git et GitHub pour permettre la gestion de version et la revue de texte (on en reparlera), AsciiDoctor pour générer un format ODT.

Il est possible d'utiliser Git, mais ça pourrait être Dropbox, c'est possible.

## Indépendance
Avant de parler d'indépendance il faut parler de dépendance. Et dans le monde du livre il y a clairement une dépendance à certains outils, comme Word ou InDesign pour les citer (qui sont clairement *obligatoires*).

On peut pousser plus loin avec l'exemple de "Adobe Digital Publishing suite - single edition" : seul ce logiciel peut lire et gérer le format produit, et lorsque le logiciel ou le service n'est plus disponible, ce qui est arrivé en 2014, le format devient inutilisable.

L'indépendance est envisageable à partir d'un environnement modulaire : choisir ses outils et les assembler. Si un logiciel n'est plus maintenu il est possible de le remplacer relativement facilement. Par exemple si vous utiliser un générateur de site statique pour générer vos différents formats de livre, vous pouvez *relativement facilement* passer de Jekyll à Hugo.

En terme d'indépendance il y a une bande de designers qui cherchent des solutions pour se passer de logiciels (propriétaires ou libres), et construire un workflow : PrePostPrint !

## Convivialité
Tous le monde est le ou la bienvenue, l'accès aux outils est identique pour chacun et chacune. L'environnement est *ouvert* plutôt que cloisonné ou fermé.

Par exemple lors de l'écriture du livre de Thomas, les relecteurs de l'éditeur ont pu corriger le texte tout simplement en modifiant un fichier Asciidoc, sans se soucier du logiciel utilisé ou de sa version (par exemple Word vs LibreOffice, avec quels styles par défaut, etc.).

En parlant de PrePostPrint on peut évoquer le fait que certains studios de graphisme réalisent des livres ou des brochures en HTML pour permettre aux clients d'intervenir *directement* dans le contenu et sur la forme.

## Mixer
Possibilité de modifier les rôles : chacun ou chacune peut intervenir sur le texte à n'importe quel moment de la chaîne d'édition. Au-delà de la convivialité c'est l'idée d'une inversion possible des rôles, ou au moins d'une compréhension de chacun à n'importe quel moment.

Décloisonner les étapes de conception et de fabrication d'un livre.

## Publication en continu
Écrire souvent et publier vite !

Ce n'est pas nouveau, par exemple avec les pré-publications de livre sous forme de feuilleton dans des journaux ou des magazines. Et même dans le domaine de la publication académique avec les pre-prints : plutôt que d'attendre la publication d'un article qui peut prendre jusqu'à presque un an, mettre à disposition un article non validé qui permettra de partager des premiers résultats.

L'idée c'est de faire circuler un texte, des contenus, *avant* l'édition finale, et pas forcément dans le circuit classique du livre. Autre exemple : faire relire des morceaux de mémoire ou de thèse en générant des formats transmissibles comme l'EPUB ou le PDF.

## Automatisation
Publier en continu nécessite d'automatiser certaines tâches, comme la génération des versions web, PDF, EPUB ou ODT/DOC.

Beaucoup d'outils d'intégration continue permettent d'automatiser la génération et la vérification des fichiers. Par exemple en utilisant le format Asciidoc et l'outil AsciiDoctor on peut automatiser la génération de PDF à chaque commit ou pull request sur un dépôt.

En automatisant on sépare aussi certains contenus, par exemple le texte et les exemples.

## Revue en continu
Publier souvent pour corriger fréquemment. Par exemple en poussant les textes sur une plateforme comme GitHub permet de recueillir des retours, des corrections.

Exemple de la revue Distill.pub qui ouvre la possibilité aux lecteurs de faire des retours (issues, pull requests), et il est possible de voir les modifications de l'article au fil du temps.

Exemple de la rédaction de NodeJS, le fait de rendre les textes disponibles sur GitHub et sur un site web dédié a permis de recevoir les retours (en continu) de l'éditrice.

Question pour Thomas : est-ce que cela ne génère pas aussi beaucoup de travail (gestion des tickets, intégration des corrections, doublons) ?

## Mutations
Depuis l'invention de l'imprimerie le livre est en constante évolution, on peut prendre l'exemple du livre de poche, les techniques d'impression, les circuits de distribution, le livre numérique, etc.

Par exemple on constate un double phénomène de sous-traitance et de précarisation des sous-traitants du livre : les graphistes sont par exemple désormais des indépendants et non plus des salariés de la structure d'édition, et ces *sous-traitants* sont totalement contraints par les rythmes de publication des éditeurs, et globalement de moins en moins bien payés.

Dissémination, explosion, sous-traitance.

## Fonctionnel
Le livre papier est fonctionnel : transportable, maniable, lisible hors ligne et sans énergie, etc. Mais une version numérique peut intégrer d'autres éléments fonctionnels, permettant au lecteur de passer de passif à actif (utile pour un livre-manuel) :

Pour cela on peut prendre deux exemples :

- la revue Distill.pub qui intègre des visualisations de données modifiables par le lecteur, ce qui permet de mieux comprendre un article ;
- le livre NodeJS qui intègre du code exécutable : le livre peut être installé pour tester du code en direct ;
- et on pourrait imaginer la même chose avec un livre imprimé : sortir du livre papier (confortable à lire) pour aller tester des éléments.

## Distribution
Dans le domaine du livre, le nerf de la guerre c'est la distribution (acheminement) et la diffusion (communication).

Faire un livre n'est pas compliqué en soit, mais le faire connaître et éventuellement le vendre est beaucoup plus complexe.

Commit Strip ou *Become a Ninja with Angular* sont des exemples de succès d'auto-distribution assez rares et isolés.

## Éditeur
La question pourrait être, après ce que l'on a vu, pourquoi ne pas se passer d'éditeur ? Dans l'auto-édition il y a de beaux succès, comme Commit Strip, mais c'est finalement assez rare (Commit Strip a créé sa propre communauté, c'est un éditeur en soit).

L'éditeur, en plus de prendre le risque financier d'un livre, d'inscrire le livre ou la revue dans une collection, de gérer le texte et la fabrication du livre, il rend un texte légitime. L'éditeur est à la fois une marque identifiable, mais aussi une garantie, une légitimité.

## Autorité
Si livre imprimé, alors reconnaissance. La publication sous une forme donne lieu à d'autres activités.

Exemples de Ninja Squad avec leur livre sur Angular, écrit aussi en Asciidoc : à partir d'une version numérique ils ont développé des services (formation), sans passer par un éditeur (mais ils sont dans une niche).

## Multi-disciplinarité
Multidisciplinarité : dans le monde du livre il y a une étanchéité entre les tâches, les fonctions et les personnes (derrière les fonctions et les personnes). Permettre la multidisciplinarité c'est diminuer le bus factor et supprimer la dette technique. Chacun et chacune apprend en voyant faire les autres.

Et en parlant de dette technique, il faut lire le livre de Bastien Jaillot *La dette technique* publié chez Le train de 13h37, qui cherche justement à concevoir et fabriquer des livres avec les concepts et outils présentés. Pour l'instant Le train de 13h37 a ouvert des dépôts pour recueillir des retours de lecteur.

## Énergie
Énergie : écrire par fragments, "un readme prend moins de temps qu'une documentation".

{distinction entre livre (format clot) et documentation (écriture continue), livrer plus tôt.}
