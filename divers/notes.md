# Note des échanges entre Thomas et Antoine

Codeurs en Seine 2017
> Une performance d'Antoine Fauchié et de Thomas Parisot.

## Étape 1 : Quelques sujets de réflexion autour du livre numérique

### Écrire avec le numérique
#### Description
L'influence du (livre) numérique dans les processus de création, de conception, de fabrication et de production du livre.

#### Exemple(s)
L'écriture du livre de Thomas.

#### Intérêt pour le public
Comprendre comment l'industrie du livre est en train de se reconfigurer, et quelles frictions cela peut entraîner.


### Le web au service du livre ?
#### Description
Comment les méthodes et les technologies du développement web influencent un domaine pourtant bien éloigné : le livre.

#### Exemple(s)
Aborder Git et HTML5 en passant par des technologies de front et des méthodes dites *agile*.)

#### Intérêt pour le public
Découvrir comment des technologies utilisées pour développer des sites web ou des applications servent également à une industrie fort éloignée.


## Étape 2 : Émergence d'un sujet commun

### Contenu
Le livre est un point de départ intéressant, il parle à tout le monde et nous sommes légitimes pour aborder ce sujet. C'est le phare qui attire l'œil ! (Attirer vers le savoir plutôt que pousser vers le savoir)

Partir du livre comme point d'accroche commun, tous les métiers.  
Écrire du contenu en ligne, avec les technos du web.  
Fabriquer un livre avec les méthodes du web (agile).  
Redescendre.

### Scénarisation, jeu de seine
Comment exploiter nos deux profils ? Monsieur livre et monsieur code ? Trouver le moyen de faire des aller-retour *pendant* la conférence.

Chercher la façon de présenter : être dans le public ? jouer avec l'espace physique ? finir dans le public avec l'idée que le livre est une figure imposante qui commence sur la scène.


## Étape 3 : déterminer le sujet commun

### Titre
Comment le web réinterroge le livre ?

### Description
Comment le livre peut être conçu et produit autrement en s'inspirant du web ? Et comment le web peut observer le livre et l'édition pour y retenir quelques principes clés (intérêt du contenu, travail d'édition) ?

Déroulé :

1. le livre est une figure particulière de nos cultures, tant en terme d'objet de consommation que de façon de concevoir des contenus ;
2. malgré les apparences, il y a beaucoup de liens entre le web et le livre (contenus organisés : le web est avant tout pensé comme un ensemble de documents organisés) ;
3. comment un livre est conçu ? Sous-entendus : méthodes classiques, souvent douloureuses ou pénibles sur certaines parties, énergie et temps perdus, etc. Mais figures de l'éditeur ou de l'auteur qui permet
4. comment un livre *pourrait* être conçu ? Sous-entendu : s'inspirer des méthodes du web, etc.
5. comment le livre peut s'inspirer du web ? et comment le web peut se réinterroger face au livre ?

## Notes du jeudi 3 août 2017

![2017-08-03-schema-livre-antoine.jpg](2017-08-03-schema-livre-antoine.jpg)

- pyramide inversée
- notion de plaisir (écriture)
- format (exemple d'Asciidoc avec des includes et l'intégration de bouts de code fonctionnel)
- outillage + tooling : Asciidoctor est par exemple modulaire (Asciidoc + NodeJS), inverser la pyramide avec la possibilité pour (presque) tous le monde de modifier et d'adapter l'outil
- qu'est-ce qu'un livre si le livre (papier) n'intègre pas les bouts de code exécutables ?
- plus on descend la pyramide plus on s'approche du public
- conclusion : le livre en soit n'est pas intéressant, mais les *patterns* entre les étages de la pyramide
- "un package peut être plus lu qu'un livre"
- pyramides : production, effort ou consommation
- readme.book


## Notes retrospectives

- faire un retour avec Delphine M sur les interfaces pour git diff ;
- mode de préparation : plus agréable et efficace en synchrone ;
- insister plus sur le mode de présentation : explications sur le format de la conférence.
