# README.book : Repenser les chaînes de publication par l'intégration des pratiques du développement logiciel
Une intervention et un article de [Thomas](https://oncletom.io/) et [Antoine](https://www.quaternum.net).

## Intervention à Codeurs en Seine
Codeurs en Seine est une journée de conférences qui rassemble des développeurs chaque année à l'automne et à Rouen.

Thomas Parisot (@oncletom) et Antoine Fauchié (@antoinentl) ont proposé un temps de présentation et de discussion autour des outils de publication, jeudi 23 novembre 2017 : comment le web influence le livre, et comment le livre peut influencer le web ?

Le livre représente un idéal certain, même en 2017 : écrire un livre c'est bien, être publié c'est bien et ça fait de nous quelqu'un·e de bien.
En tous cas vu de loin, ça a l'air bien mieux que d'écrire le README d'un projet ou sa documentation, un papier scientifique ou encore un rapport qu'on nous a demandé pour avant-hier.
L'écriture d'un README et celle d'un livre sont-elles si différentes que ça ?
Du numérique au papier, du stylo au clavier et d'un commit git au fichier ODT, nous verrons comment infuser la culture du web dans l'édition via des contenus et des formats ouverts, les pratiques collaboratives, entre autres.

La vidéo de cette intervention est disponible en ligne : [https://www.youtube.com/watch?v=25wCiZVLNBg](https://www.youtube.com/watch?v=25wCiZVLNBg)

## Article dans la revue Sciences du design
Thomas et Antoine ont répondu à un appel à contribution de la revue [Sciences du design](http://www.sciences-du-design.org/), avec comme objectif de prolonger les réflexions engagées lors de Codeurs en Seine avec les contraintes d'une revues universitaires.

Cet article, intitulé "Repenser les chaînes de publication par l'intégration des pratiques du développement logiciel", a été écrit avec les concepts présentés dans le texte.
Les contenus sont donc versionnés sur ce dépôt, la source est au format AsciiDoc, un langage de balisage léger, et différentes versions/formats sont automatiquement générées, dont [une version web](https://antoinentl.gitlab.io/readme.book/).

La bibliographie est intégrée dans le dépôt au format BibTeX, il s'agit d'une extraction du groupe Zotero : [https://www.zotero.org/groups/2155559/sciences-du-design-chaines-de-publication](https://www.zotero.org/groups/2155559/sciences-du-design-chaines-de-publication)

Cet article a été publié mercredi 5 décembre 2018 dans le numéro 8 de la revue Sciences du Design en [version imprimée](https://www.puf.com/content/Sciences_du_Design_2018_8) et en [version numérique](https://www.cairn.info/revue-sciences-du-design.htm), numéro consacré à la thématique "Éditions numériques".
Des retours suite à la publication de cet article peuvent être transmis via ce dépôt, en créant [une nouvelle issue](https://gitlab.com/antoinentl/readme.book/issues/new).
