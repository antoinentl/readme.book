---
date: 2017-11-28
---

# Rétrospective

Cette rétrospective fait suite à notre présentation en duo sur les chaînes de publications numériques au format texte à la conférence Codeurs en Seine à Rouen le 23 novembre 2017.

## Ce qui a bien marché

- nous nous sommes bien débrouillés avec l'enchevêtrement des mondes de l'édition et du développement logiciel
- on a passé un bon moment
- nous nous sommes sentis à l'aise avec le rythme de la présentation
- nous avons trouvé qu'il y avait un bon équilibre entre les exemples et les retours d'expérience
- c'était une chouette expérience, on la referait !
- on était à l'aise de ne pas être en contrôle de tout le déroulé — on avait une confiance mutuelle dans notre capacité à rebondir et à aider sur les idées de l'autre
- on serait à l'aise d'apporter ce format dans un autre contexte plus strict (comme un colloque académique ou autre)
- nous avons aimé le processus de création — combinant appear.in et rencontres en personne
- nous avons aimé nous écouter et ne pas nous imposer quoi que ce soit
- nous avons aimé le concept de slides — et nous de développer nos idées autour des concepts qu'ils illustrent
- ces moments autours (bouffe, Airbnb)

## Ce qui a moins bien marché

- je ne me suis pas senti clair dans l'explication du contexte/cadre
- c'était étrange de parler devant une salle peu remplie — le créneau de trop ?
- j'ai oublié des exemples de concepts, ils n'étaient pas documentés dans les notes de présentation
- on avait commencé à bosser sur draft.in mais workflow auteur/correcteur et non collaborateurs
- on avait commencé à créer le contenu sur GitLab mais que c'était pas le plus efficace pour collaborer (les appels appear.in avaient plus de valeur)
