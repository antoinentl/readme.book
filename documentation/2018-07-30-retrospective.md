---
date: 2018-07-30
duration: P2H
---

# Rétrospective

Cette rétrospective fait suite à la soumission puis la proposition et la révision d'un article pour la revue académique Sciences du design.

## Qu'est-ce qui a bien marché ?

- l'idée de départ, de proposer un article et de travailler à 2 dessus
- trouver une autre utilisation concrète d'Asciidoc, qui ne soit pas un livre, avec une gestion des trucs un peu compliqués (comme la bibliographie)
- la gestion à distance a bien fonctionné avec notamment Teletype dans Atom, la méthode appliquée pour écrire l'article est la même que celle défendue dans le texte : format léger, outils limités mais répondant à nos besoins ;
- avoir été retenus et être allés jusqu'au bout, le fait d'avoir été deux a été un bon moteur ;
- faire ce texte à deux (support moral 👍) ;
- setup et les choix techniques : Atom+Teletype, GitLab+Gitlab CI, Zotero, AsciiDoc+Asciidoctor+Pandoc
- ce qu'on a produit : du flux, du sujet, comment on l'a verbalisé
- texte riche : exemples et références bibliographiques qui ont donné une autre dimension
- Lucile : cela aurait été différent sans elle, elle a été très disponible et elle a défendu le texte, et chouette rencontre pour Thomas
- relectures extérieures : surtout Noémie, Michel (père d'Antoine)

## Qu'est-ce qui a moins bien marché ?

- le temps que ça a pris — on avait sous-estimé le temps nécessaire même si on avait prévu que ça en prenne beaucoup (1 à 2 semaines chacun)
- les deux styles d'écriture différents n'ont pas trop plu au moment de la relecture
- le manque de disponibilité / de souplesse
- provisionner du temps dans la durée, et à des moments précis du projet (relectures, corrections)
- le manque de règles claires sur les consignes de rédaction, les styles (d'écriture), effet de déception (c'est fini ? Ben non on repart encore dans des corrections)
- passer de toutes les idées à quelque chose de très structuré et consolidé, c'est plus compliqué que pour une communication (intervention), et c'est une question de gestion de temps
- il a fallu reformuler beaucoup de choses alors qu'on avait mis beaucoup d'énergie dedans — c'est moins personnel, on se fond complètement dans leur cadre
- frustration d'une collaboration tardive avec l'équipe de la revue : pas d'accompagnement dans la première phase, envies d'avoir des relectures intermédiaires avant la relecture anonymisée, afficher les règles et l'organisation, mode compétitif par défaut
- difficulté de travailler avec trois personnalités différentes non accordées entre elles, il aurait fallu un lead pour avoir un seul retour

## Qu'est-ce qu'on ne referait plus ?

- commencer sans connaître les règles (spécifiques à la publication)
- avoir autant de sujets d'écriture en même temps (livre, mémoire, colloques, articles), effort pour passer d'un projet à un autre
- travailler autant en ayant un feedback aussi tardif : les itérations étaient trop longues !
- avoir un _scope_ aussi large : il aurait fallu nous imposer un cadre plus restreint pour ne pas être obligé de parler de tout
- relectures dans deux outils différents

## Qu'est-ce qu'il faudrait refaire ?

- modifier les règles des revues pour attirer de nouveaux profils hors chercheurs universitaires
- continuer à faire des trucs à deux
- continuer à explorer cette voie
- construire un système (ici un assemblage Lego)
- travailler en mode _émergent_ : on crée un outil personnalisé sans chercher à adopter un outil existant et à nous contraindre
- le setup et les choix techniques (ouverture d'AsciiDoc et d'Asciidoctor notamment)
- continuer à montrer, documenter et faire de la pédagogie

## Qu'est-ce qui reste encore mystérieux ?

- pourquoi ça m'a autant coûté (émotionnellement) d'écrire
- où est la fluidité dans ce type de contexte ?
- le fonctionnement de l'équipe qui a travaillé sur notre article : quelle marge ? quelle organisation ? quel lead ? quelle marge de manœuvre pour nous ?

## Qu'est-ce qu'on a appris ?

- différences entre une référence bibliographique et une dépendance pour un programme
- rentrer dans un cadre
- GitLab Pages et le mécanisme de pipelines
- tout ce qui tourne autour de la bibliographie (Zotero, Bibtex)
- gestion et application des styles de documents dans Pandoc pour des formats .docx ou .odt
- références sur les dynamiques de systèmes de Donella Meadows
- j'ai un peu mieux compris ce qu'on pouvait faire avec Pandoc
- l'écriture à deux — il faut laisser de la place
- écrire 40 000 signes c'est facile — 25 000 c'est plus dur
- c'est plus facile de maintenir un enthousiasme à deux, que tout seul

# Actions

- mettre un dépôt qui illustre ces propos, en accès libre
- chercher un moyen de récupérer un export automatisé d'une bibliographie : à partir d'une URL. Interroger les utilisateurs sur le forum !
- angle d'attaque intéressant pour les archivistes : produire des fichiers de traitement de texte ou de présentation sans avoir à en ouvrir (application de feuille de styles)
- proposer un sujet ou deux avec des angles différents dans la continuité de ce travail, par exemple au [forum des Archivistes](https://www.archivistes.org/-Forum-des-archivistes-693-) ou Paris-Web 2019 (sous l'angle outillage/texte/HTML5/CSS vers épreuve papier)
- reporter les modifications du Google Doc dans la source Asciidoc
- envoyer la version web de l'article à Roxane de publie.net
- proposer un call à Guillaume
